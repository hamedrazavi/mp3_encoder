#include "mp3encoder.hpp"

mp3encoder::mp3encoder(){}

static int write_id3v1_tag(lame_t gf, std::ofstream &of)
{
    unsigned char mp3buffer[128];
    size_t  imp3;

    imp3 = lame_get_id3v1_tag(gf, mp3buffer, sizeof(mp3buffer));
    if (imp3 <= 0) {
        return 0;
    }
    if (imp3 > sizeof(mp3buffer)) {
        printf("Error writing ID3v1 tag: buffer too small: buffer size=%lu ID3v1 size %zu", sizeof(mp3buffer), imp3);
        return 0;       /* not critical */
    }
    of.write((char *) mp3buffer, imp3); // ToDo: Check if writing was successfull owrite == imp3
    return 0;
}

static int write_xing_frame(lame_global_flags * gf, std::ofstream &of, size_t offset)
{
    unsigned char mp3buffer[LAME_MAXMP3BUFFER];
    size_t  imp3;

    imp3 = lame_get_lametag_frame(gf, mp3buffer, sizeof(mp3buffer));
    if (imp3 <= 0) {
        return 0;       // nothing to do
    }

    if (imp3 > sizeof(mp3buffer)) {
        printf("Error writing ID3v1 tag: buffer too small: buffer size=%lu ID3v1 size %zu", sizeof(mp3buffer), imp3);
        return -1;
    }
    // assert( offset <= LONG_MAX ); //ToDo
    // if (fseek(outf, (long) offset, SEEK_SET) != 0) {
    //     error_printf("fatal error: can't update LAME-tag frame!\n");
    //     return -1;
    // }
    of.write((char *) mp3buffer, imp3); // ToDo: check if writing was successfull
    // assert( imp3 <= INT_MAX );
    return (int) imp3;
}

lame_t mp3encoder::initialize() 
{
    lame_ = lame_init();
    return lame_;
}

int mp3encoder::convert(audio_file &af, std::ofstream &of) 
{
    // initialize the id3 tag 
    id3tag_init(lame_);
    //lame_set_write_id3tag_automatic(lame_, 0); // ToDo double check

	// set lame parameters, e.g. quality, num channels, etc. 
    set_lame_parameters(af);
    size_t id3v2_size = lame_get_id3v2_tag(lame_, 0, 0); // ToDo double check

    int pcm_buffer[2][1152];
    unsigned char mp3_buffer[LAME_MAXMP3BUFFER];
    int samples_read = 0;
    int imp3 = 0;
    int samples_to_read = af.frame_size_;
    unsigned int remaining = af.num_samples_;
    
    // read pcm buffers and encode to mp3 buffers
    do {
        samples_to_read = af.frame_size_; 
        remaining -= samples_read;
        if (remaining < (unsigned int) af.frame_size_ && af.num_samples_ != 0) {
            samples_to_read = remaining;
        }
        // This is were the main job of reading samples to pcm buffer happens
        samples_read = af.get_samples(pcm_buffer, samples_to_read);

		const int *buffer_l = pcm_buffer[0];
        const int *buffer_r = pcm_buffer[1];
        const int chunk = samples_read;

        // start encoding
        imp3 = lame_encode_buffer_int(lame_, buffer_l, buffer_r, chunk, mp3_buffer, sizeof(mp3_buffer));
        of.write((char *) mp3_buffer, imp3);
    } while (samples_read > 0);

    imp3 = lame_encode_flush(lame_, mp3_buffer, sizeof(mp3_buffer)); // may return one more mp3 frame 
    of.write((char *) mp3_buffer, imp3); // Tocheck
    imp3 = write_id3v1_tag(lame_, of); // ToCheck
    if (imp3) {
        lame_close(lame_);
        return 1;
    }
    write_xing_frame(lame_, of, id3v2_size);
    lame_close(lame_);
    return 0;
}

void mp3encoder::set_lame_parameters(audio_file &af) 
{
    lame_set_num_channels(lame_, af.num_channels_);
    lame_set_in_samplerate(lame_, af.sample_rate_);
    lame_set_num_samples(lame_, af.num_samples_);
    lame_set_VBR(lame_, vbr_default); 
    lame_set_quality(lame_, 5); // good quality, still fast enough
    lame_init_params(lame_);    
}