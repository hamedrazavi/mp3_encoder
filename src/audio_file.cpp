#include <string>
#include <cstring>
#include <cassert>
#include <limits.h>
#include "audio_file.hpp"

audio_file::audio_file(std::string path)
{
    path_ = path;
    is_.open(path_, std::ifstream::binary);
}

audio_file::audio_file() {}

static long make_even_number_of_bytes_in_length(long x)
{
    if ((x & 0x01) != 0) {
        return x + 1;
    }
    return x;
}

int audio_file::get_samples(int pcm_buffer[2][1152], int samples_to_read)
{
    int samples_read = 0;
    int in_sample[2 * 1152];
    int *p, i;
    samples_read = read_samples_pcm(in_sample, num_channels_ * samples_to_read);
    p = in_sample + samples_read;
    samples_read /= num_channels_;

    if (pcm_buffer != NULL)
    { // output to int buffer //
        if (num_channels_ == 2) {
            for (i = samples_read; --i >= 0;) {
                pcm_buffer[1][i] = *--p;
                pcm_buffer[0][i] = *--p;
            }
        }
        else if (num_channels_ == 1) {
            memset(pcm_buffer[1], 0, samples_read * sizeof(int));
            for (i = samples_read; --i >= 0;) {
                pcm_buffer[0][i] = *--p;
            }
        }
        else
            assert(0);
    }
    return samples_read;
}

int audio_file::read_samples_pcm(int samples_buffer[2304], int all_samples_to_read)
{
    int samples_read;
    int bytes_per_sample = bits_per_sample_ / 8;
    int swap_byte_order = 0; // byte order of input stream //
    switch (bits_per_sample_) {
    case 32:
    case 24:
    case 16:
        break;
    case 8:
        swap_byte_order = 1;
        break;

    default:
        printf("Only 8, 16, 24 and 32 bit input files supported \n");
        return -1;
    }
    samples_read = unpack_read_samples(all_samples_to_read, bytes_per_sample, swap_byte_order,
                                       samples_buffer);
    return samples_read;
}

int audio_file::unpack_read_samples(const int samples_to_read, const int bytes_per_sample,
                                    const int swap_order, int *sample_buffer)
{
    int samples_read;
    int i;
    int *op;                                            /* output pointer */
    unsigned char *ip = (unsigned char *)sample_buffer; /* input pointer */
    const int b = sizeof(int) * 8;
    {
        is_.read((char *)sample_buffer, bytes_per_sample * samples_to_read);
        size_t samples_read_ = is_.gcount() / bytes_per_sample;
        assert(samples_read_ <= INT_MAX);
        samples_read = (int) samples_read_;
    }
    op = sample_buffer + samples_read;

#define GA_URS_IFLOOP(ga_urs_bps)       \
    if (bytes_per_sample == ga_urs_bps) \
        for (i = samples_read * bytes_per_sample; (i -= bytes_per_sample) >= 0;)

    if (swap_order == 0) {
        GA_URS_IFLOOP(1)
        *--op = ip[i] << (b - 8);
        GA_URS_IFLOOP(2)
        *--op = ip[i] << (b - 16) | ip[i + 1] << (b - 8);
        GA_URS_IFLOOP(3)
        *--op = ip[i] << (b - 24) | ip[i + 1] << (b - 16) | ip[i + 2] << (b - 8);
        GA_URS_IFLOOP(4)
        *--op =
            ip[i] << (b - 32) | ip[i + 1] << (b - 24) | ip[i + 2] << (b - 16) | ip[i + 3] << (b - 8);
    }
    else {
        GA_URS_IFLOOP(1)
        *--op = (ip[i] ^ 0x80) << (b - 8) | 0x7f << (b - 16); /* convert from unsigned */
        GA_URS_IFLOOP(2)
        *--op = ip[i] << (b - 8) | ip[i + 1] << (b - 16);
        GA_URS_IFLOOP(3)
        *--op = ip[i] << (b - 8) | ip[i + 1] << (b - 16) | ip[i + 2] << (b - 24);
        GA_URS_IFLOOP(4)
        *--op =
            ip[i] << (b - 8) | ip[i + 1] << (b - 16) | ip[i + 2] << (b - 24) | ip[i + 3] << (b - 32);
    }
#undef GA_URS_IFLOOP
    if (format_tag_ == WAVE_FORMAT_IEEE_FLOAT) {
        ieee754_float32_t const m_max = INT_MAX;
        ieee754_float32_t const m_min = -(ieee754_float32_t)INT_MIN;
        ieee754_float32_t *x = (ieee754_float32_t *)sample_buffer;
        assert(sizeof(ieee754_float32_t) == sizeof(int));
        for (i = 0; i < samples_to_read; ++i) {
            ieee754_float32_t const u = x[i];
            int v;
            if (u >= 1) {
                v = INT_MAX;
            }
            else if (u <= -1) {
                v = INT_MIN;
            }
            else if (u >= 0) {
                v = (int)(u * m_max + 0.5f);
            }
            else {
                v = (int)(u * m_min - 0.5f);
            }
            sample_buffer[i] = v;
        }
    }
    return (samples_read);
}

int audio_file::parse_header()
{
    if (get_format() == wav) {
        return parse_wave_header();
    }
    else {
        return -1;
    }
}

int audio_file::parse_wave_header()
{
    (void)read_32_high_low(is_); /*file length - 8 bytes*/
    if (read_32_high_low(is_) != WAV_ID_WAVE)
        return -1;
    unsigned long subsize = 0, data_length = 0;
    int is_wav = 0;

    int i = 0;
    for (i = 0; i < 20; i++) {
        int type = read_32_high_low(is_);
        if (type == WAV_ID_FMT) {
            subsize = read_32_low_high(is_);                        // "fmt" subchunk size 
            subsize = make_even_number_of_bytes_in_length(subsize); // if odd add 1 to make it even 
            // check if subchunk is too short
            if (subsize < 16)
                return -1;
            format_tag_ = read_16_low_high(is_); /* 2 bytes of Audioformat: if not 1, then is compressed */
            subsize -= 2;
            num_channels_ = read_16_low_high(is_); /* nubmer of channels */
            subsize -= 2;
            sample_rate_ = read_32_low_high(is_); /* samplerate */
            subsize -= 4;
            frame_size_ = sample_rate_ > 24000 ? 1152 : 576;
            is_.ignore(6); /* ignore bytes per second and block align */
            subsize -= 6;
            bits_per_sample_ = read_16_low_high(is_);
            subsize -= 2;
            /* Extensible support */
            if ((subsize > 9) && (format_tag_ == WAVE_FORMAT_EXTENSIBLE)) {
                is_.ignore(8);
                format_tag_ = read_16_low_high(is_);
                subsize -= 10;
            }
            if (subsize > 0) {
                //--> if ? then return -1 //ToDo
            }
            is_.ignore(subsize); /* ignore the rest of fmt block */
        }
        else if (type == WAV_ID_DATA) {
            data_length = read_32_low_high(is_);
            is_wav = 1;
            break; // audio data found //
        }
        else {
            subsize = read_32_low_high(is_);
            subsize = make_even_number_of_bytes_in_length(subsize); /* if odd add 1 to make it even */
            is_.ignore(subsize);
            // return -1; // ToCheck
        }
    }
    if (is_wav) {
        if (format_tag_ == 0x0050 || format_tag_ == 0x0066)
            return 7; //sf_mp123
        if (format_tag_ != WAVE_FORMAT_PCM && format_tag_ != WAVE_FORMAT_IEEE_FLOAT) {
            return -1;
        }
        if (data_length == MAX_U_32_NUM)
            num_samples_ = MAX_U_32_NUM;
        else
            num_samples_ = data_length / (num_channels_ * ((bits_per_sample_ + 7) / 8));
        return 0;
    }
    return -1;
}

int audio_file::read_32_high_low(std::ifstream &is)
{
    unsigned char bytes[4] = {0, 0, 0, 0};
    is.read((char *)bytes, 4);
    int32_t const low = bytes[3];
    int32_t const medl = bytes[2];
    int32_t const medh = bytes[1];
    int32_t const high = (signed char)(bytes[0]);
    return (high << 24) | (medh << 16) | (medl << 8) | low;
}

int audio_file::read_32_low_high(std::ifstream &is)
{
    unsigned char bytes[4] = {0, 0, 0, 0};
    is.read((char *)bytes, 4);
    int32_t const low = bytes[0];
    int32_t const medl = bytes[1];
    int32_t const medh = bytes[2];
    int32_t const high = (signed char)(bytes[3]);
    return (high << 24) | (medh << 16) | (medl << 8) | low;
}

int audio_file::read_16_low_high(std::ifstream &is)
{
    unsigned char bytes[2] = {0, 0};
    is.read((char *)bytes, 2);
    int32_t const low = bytes[0];
    int32_t const high = (signed char)(bytes[1]);
    return (high << 8) | low;
}

int audio_file::get_format()
{
    if (read_32_high_low(is_) == WAV_ID_RIFF) {
        format_ = wav;
    }
    else if (read_32_high_low(is_) == IFF_ID_FORM) {
        format_ = aiff;
    }
    else {
        format_ = other;
    }
    return format_;
}

void audio_file::close()
{
    is_.close();
}