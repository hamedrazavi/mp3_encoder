#include "encode_file.hpp"

string remove_file_ext(string file_name) {
    size_t last_index = file_name.find_last_of(".");
    return file_name.substr(0, last_index);
}

int check_if_supported (audio_file &af) { // ToDo complete this function with more cases
	
    size_t last_index = af.path_.find_last_of("/");
    string file_name = af.path_.substr(last_index + 1, af.path_.length());
    // Check if audio format is wav
    if (af.format_ != wav) {
        printf("%s -> not a valid wave audio file.\n", file_name.c_str());
        return -1;
    }
    // Check if the number of channels is supported
    if (af.num_channels_ < 1 || af.num_channels_ > 2) {
        printf("%s -> unsupported number of audio channels.\n", file_name.c_str());
        return -1;
    }
    // Check if the format tag data is supported
    if (af.format_tag_ != WAVE_FORMAT_PCM && af.format_tag_ != WAVE_FORMAT_IEEE_FLOAT) {
        printf("%s -> unsupported data format: 0x000%d.\n", file_name.c_str(), af.format_tag_);
        return -1;
    }
    return 0;
}

int encode_file(audio_file &af) {
    int ret_parse = af.parse_header();  // parse the audio_file
    // check if the audio is supported and print appropriate message if possible
    int ret = check_if_supported(af); 

    if (ret_parse)
         return -1;
     if (ret == -1)
         return -1;

    // prepare an output file 
    string out_file_name = remove_file_ext(af.path_);
    std::ofstream of((out_file_name + ".mp3").c_str(), std::ofstream::binary); 

    mp3encoder encoder; 
    // call lame_init()
    if (encoder.initialize() == NULL) { 
        printf("Fatal error during initialization");
        return 1;
    }
    
    encoder.convert(af, of); // convert to mp3 and save
    of.close();
    return 0;
}