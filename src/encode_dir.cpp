
/*
Includes the 'main'. It gets a directory path and encodes the wave files in
the directory to mp3 using lame library. The encoding is done with variable 
bit rate setting of the lame library and a quality level of 'good'. The enc-
oding is perfomed with multi-threading to maximize the speed of conversion.
The application is compilable and runnable in unix and windows OS. 

Example:
./lamedir <path/to/directory>

Author: Hamed Razavi (razavi@umich.edu)
*/

#include <stdio.h>
#include <string>
#include <cstring>
#include <vector>
#include <set>
#include <algorithm>
#include "audio_file.hpp"
#include "encode_file.hpp"
#include "dirent.h"
#include "pthread.h"

using namespace std;

void *encode_file_thread(void *arg)
{
    string *full_path;
    full_path = (string *) arg;
    audio_file af(*full_path);
    int ret = encode_file(af);   // encode the given file
    size_t last_index = full_path->find_last_of("/"); 
    string file_name = full_path->substr(last_index + 1, af.path_.length());
    if (!ret)
        printf("%s -> converted to mp3.\n", file_name.c_str());
    fflush(stdout);
    af.close();
    pthread_exit(NULL);
	return 0;
}


int get_list_of_files(char *dir_path, vector<string> &files_list, bool &duplicate_names_flag)
{
    DIR *dir;
    struct dirent *ent;
    char *file_name;
    string full_path;
    set<string> files_names_list;
    // get the file paths and add them to the files_list
    if ((dir = opendir(dir_path)) != NULL) {
        while ((ent = readdir (dir)) != NULL) {
            file_name = ent->d_name;
            if ((strcmp(file_name, ".")) && (strcmp(file_name, ".."))) {
                full_path = dir_path;
                if (full_path.back() != '/') 
                    full_path.append("/");
                full_path.append(file_name);
                files_list.push_back(full_path);
                string file_name_no_ext = file_name;
                file_name_no_ext = remove_file_ext(file_name_no_ext);
                if ((find(files_names_list.begin(), files_names_list.end(), file_name_no_ext) != files_names_list.end())
                    && (duplicate_names_flag == false)) {
                    duplicate_names_flag = true;
                }
                if (duplicate_names_flag == false)
                    files_names_list.insert(file_name_no_ext);
            }
        }
        closedir (dir);
    }
    else {
        perror("");
        return EXIT_FAILURE;
    }
	return 0;
}

int main(int argc, char *argv[]) 
{    
    // if no argument is given exit
     if (argc == 1) {
        printf("\nPlease specify the directory of WAV files, e.g, ./encode <path_to_WAV_files_directory>.\n\n");
        return 1;
    }

    // get the list of files in the directory
    char *dir_path = argv[1];   
    vector<string> files_list;
    bool duplicate_names_flag = false;    
    int ret = get_list_of_files(dir_path, files_list, duplicate_names_flag); // get the list of files paths in the directory dir_path
    if (ret != 0) {
        printf("Failed opening the folder \n");
        return 1;
    }

    // create as many threads as the number of files in the directory 
    int NUM_THREADS = files_list.size();
    printf("\nConverting %d files to mp3 ..\n\n", NUM_THREADS);
	pthread_t *threads = new pthread_t[NUM_THREADS];
	for (int i(0); i < NUM_THREADS; i++) {
		int rc = pthread_create(&threads[i], NULL, encode_file_thread, (void *)&files_list[i]);
		if (rc) {
			printf("Could not create thread to convert %s\n", files_list[i].c_str());
			exit(-1);
		}
	}

    // join threads
	for (int i(0); i < NUM_THREADS; i++) {
		void *status;
		int rc = pthread_join(threads[i], &status);
		if (rc) {
			printf("Error in Joining threads \n");
		}
	}
    if (duplicate_names_flag == true) {
         printf("\nWARNING: There exist dubplicate file names with different extensions. Some output mp3 files may be overwritten. \n\n");
    }
	delete[] threads;
    pthread_exit(NULL);
    return 0;
}