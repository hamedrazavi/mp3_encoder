# Lamedir
This application, **lamedir**, encodes wave audio files in a given directory to mp3 using the lame library. The encoding is done efficiently with multi-threading. The application is compilable and runnable for unix and windows OS. 

# Installation 
## Unix
### prerequisites
* **lame library** (libmp3lame)
Download lame library from [sourceforge link](https://sourceforge.net/projects/lame/files/lame/) and follow the instructions for installations on Unix systems, briefly:
<br/>./configure
<br/>make
<br/>sudo make install
<br/>
<br/>Lamedir has been built successfully for lame version 3.100 (latest version as of August 2018). 

* **pthread** (libpthread)
Pthread normally comes with Unix systems. So no further action is needed. 

### Building lamedir
Assuming you are inside this directory (where this README.md file is) 
<br/>cd . .
<br/>mkdir build
<br/>cd build
<br/>cmake . ./mp3_encoder/
<br/>make

### Example (Unix)
From the build directory:

./lamedir path/to/directory/of/audio/files/

The supported wave audio files will be converted to mp3 and stored with the same name (but with .mp3 extension) in the same folder. The quality of the mp3 files is "good" and the encoding uses variable bit rate for efficiency (see [lame usage vbr](https://svn.code.sf.net/p/lame/svn/trunk/lame/doc/html/vbr.html))

## Windows
### prerequisites
* **lame library** (libmp3lame.lib)
Download the lame lirbary (for mp3 encoding) [sourceforge link](https://sourceforge.net/projects/lame/files/lame/) and follow the instructions for installation on windows. After successfull installation you will get the libmp3lame-static.lib and lame.exe. What you need here is libm3lame-static.lib. Place this library in the lib_win folder. Also, place "lame.h" in the /include folder (note that already lame.h for version 3.100 is in /include)
<br/>
<br/>Lamedir has been built successfully for lame version 3.100 (latest version as of August 2018). 

* **pthread** 
Download and install the static library of pthread for windows (e.g. from this [github link](https://github.com/GerHobbelt/pthread-win32)). Place the static library libpthreadVC3.lib in the lib_win folder. Also place the include files of the pthread buit in lib_include_win (currenlty, these headers are pthread.h, _ptw32.h, sched.h, and semaphore.h.)

### Cmake windows (Visual Studio C++)
For building the project you need to have cmake installed. The best solution is to have visual studio installed on windows and use the Developer Community Command Prompt (once you have visual studio installed for c++, type in developer in start menu of windows). From this command prompt you can do cmake as instructed below. 

### Building lamedir
Assuming you are inside this directory (where this README.md file is) from the Visual Studio Developer Command Prompt:
<br/>cd . .
<br/>mkdir build
<br/>cd build
<br/>cmake . .\mp3_encoder\

Now open the generated solution (encode_dir.sln) in visual studio. Then go to build menu and hit 'build all'. If the build is successfull you can find the application lamedir.exe in \build\Debug.

### Example (windows)
If "path/to/audio/files" is the directory path (not an individual audio file) including the wave audio files, then from the \build\Debug\ folder type:

lamedir.exe path\to\directory\of\audio\files\

The supported wave audio files will be converted to mp3 and stored with the same name (but with .mp3 extension) in the same folder. The quality of the mp3 files is "good" and the encoding uses variable bit rate for efficiency (see [lame usage vbr](https://svn.code.sf.net/p/lame/svn/trunk/lame/doc/html/vbr.html))
