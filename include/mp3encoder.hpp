/*
mp3 encoder class, the main job of converison is performed by this class with
the convert method. 
*/
#include "lame.h"
#include "audio_file.hpp"

class mp3encoder{
    private:
        lame_t lame_;
        void set_lame_parameters(audio_file &af);
    public:
        mp3encoder();
        lame_t initialize();
        int convert(audio_file &af, std::ofstream &of);
};
