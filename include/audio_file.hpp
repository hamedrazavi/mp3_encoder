/* audio_file class: includs the audio file properties such as number of channels,
bit rate, etc. and methods including parse header and parse wave header to get the
audio properties. Some of the methods are modifications of the methods in the lame
library frontend.
*/
#ifndef AUDIO_FILE_H
#define AUDIO_FILE_H
#include <fstream> 
#include <string>
#include "lame.h"

static int const WAV_ID_RIFF = 0x52494646; /* "RIFF" */
static int const WAV_ID_WAVE = 0x57415645; /* "WAVE" */
static int const WAV_ID_FMT = 0x666d7420; /* "fmt " */
static int const WAV_ID_DATA = 0x64617461; /* "data" */

static int const IFF_ID_FORM = 0x464f524d; /* "FORM" */
#ifndef WAVE_FORMAT_PCM
static short const WAVE_FORMAT_PCM = 0x0001;
#endif
#ifndef WAVE_FORMAT_IEEE_FLOAT
static short const WAVE_FORMAT_IEEE_FLOAT = 0x0003;
#endif
#ifndef WAVE_FORMAT_EXTENSIBLE
const int WAVE_FORMAT_EXTENSIBLE = 0xFFFE;
#endif
enum format_list_{wav, aiff, other};
constexpr auto MAX_U_32_NUM = 0xFFFFFFFF;
typedef float ieee754_float32_t; // ToCheck
extern ieee754_float32_t fast_log2(ieee754_float32_t x); // ToCheck in util.h
class audio_file {
    public:
        audio_file(std::string path);
        audio_file();
        std::string path_;
        int num_samples_;
        std::ifstream is_;
        int sample_rate_;
        int bit_rate_;
        int pcm_size_;
        int num_channels_;
        bool swap_channel_;
        bool swap_bytes_;
        int bits_per_sample_;
        int frame_size_;
        short int format_; // only wav is supported 
        int format_tag_;
        // get the audio data in pcm buffers
        int get_samples(int pcm_buffer[2][1152], int sampels_to_read);
        // calls parse_wave_header if wav otherwise exits
        int parse_header();
        void close();
    private:
        int read_32_high_low(std::ifstream &is);
        int read_32_low_high(std::ifstream &is);
        int read_16_low_high(std::ifstream &is);
        // get audio format e.g. wav, aiff, etc. 
        int get_format();
        // if the audio is wav parses the header to get audio properties
        int parse_wave_header();
        int unpack_read_samples(const int samples_to_read, const int bytes_per_sample,
                                const int swap_order, int *sample_buffer);
        int read_samples_pcm(int samples_buffer[2304], int all_samples_to_read);
};
#endif